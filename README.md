#### Notes for "Module 9 - AWS Services" exercises

Deploy Module 8 exercises' NodeJS app on an EC2 instance. Create and prepare an EC2 server with the AWS CLI to run your NodeJS app container on it.

EXERCISE 1: Create IAM user with correct permissions to execute the tasks below.

- Was logged in as user `twnadmin` w/ attached policy `AdministratorAccess` to run following cmds to create a user and group w/ sufficient privileges to do these tasks.

- Create a new IAM user using "your name" as a username and "devops" as the user-group

  - `aws iam create-user --user-name upnata`
  - `aws iam create-group --group-name devops`
  - `aws iam add-user-to-group --user-name upnata --group-name devops`

- Give the "devops" group all needed permissions to execute the tasks below - with UI login and CLI credentials.

  - UI login requires password:

    - Attach a policy to `devops` group for `upnata` user to be able to reset their pw during first UI login: `aws iam attach-group-policy --group-name devops --policy-arn arn:aws:iam::aws:policy/IAMUserChangePassword`

    - Create login profile: `aws iam create-login-profile --user-name upnata --password upnata^7 --password-reset-required`

    - UI login: upnata, Bhagwati1990!

  - CLI user credentials are access key-pair:

    - Create access key-pair (access key, secret access key): `aws iam create-access-key --user-name upnata`

      - Access key: `AKIAUKVPVGSOSIYPYDFA`
      - Secret key: `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`

    - Attach a policy to `devops` group to allow its users to run:

      - EC2 cmds: `aws iam attach-group-policy --group-name devops --policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess`
      - The following was in the Solutions code, but your user didn't need this policy to do the unit.

        - VPC cmds: `aws iam attach-group-policy --group-name devops --policy-arn arn:aws:iam::aws:policy/AmazonVPCFullAccess`

      - Confirm 2 policies were added: `aws iam list-attached-group-policies --group-name devops`.

    - Configure as default user: `aws configure` with region `us-east-1`, output `json`

EXERCISE 2: Configure AWS CLI

- Configure `upnata` as default user:
  - Run `aws configure` OR individually set credentials: `aws configure set aws_access_key_id AKIAUKVPVGSOSIYPYDFA`, `aws configure set aws_secret_access_key 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`
  - Configure correct region: `aws configure set aws_default_region us-east-1`

EXERCISE 3: Create dedicated VPC using CLI (Reference: https://docs.aws.amazon.com/vpc/latest/userguide/create-vpc.html#create-vpc-cli)

- Create a new VPC with 1 subnet

  - Create new VPC given IPv4 CIDR block, returning VpcId: `aws ec2 create-vpc --cidr-block 10.0.0.0/24 --query Vpc.VpcId --output text`

    - VpcId: `vpc-0b40f5d04d3027c89`

  - Create 1 subnet, providing VpcId, returning SubnetId: `aws ec2 create-subnet --vpc-id vpc-0b40f5d04d3027c89 --cidr-block 10.0.0.0/25 --availability-zone us-east-1c --query Subnet.SubnetId --output text`

    - SubnetId: `subnet-09dab358e9c6d85b5`

    - Alternative method to get info on subnets in a VPC: `aws ec2 describe-subnets --filters "Name=vpc-id, Values=vpc-031486fd22739d293"`

  - To make subnet public:

    - Create 1 Internet gateway, returning InternetGatewayId: `aws ec2 create-internet-gateway --query InternetGateway.InternetGatewayId --output text`

      - InternetGatewayId: `igw-0222afa6c521d6443`

    - Attach gateway to VPC: `aws ec2 attach-internet-gateway --vpc-id vpc-0b40f5d04d3027c89 --internet-gateway-id igw-0222afa6c521d6443`

    - Create route table for VPC, returning RouteTableId: `aws ec2 create-route-table --vpc-id vpc-0b40f5d04d3027c89 --query RouteTable.RouteTableId --output text`

      - RouteTableId: `rtb-0c48fa00ce3591cef`

    - Create route in table sending all IPv4 traffic to gateway / VPC: `aws ec2 create-route --route-table-id rtb-0c48fa00ce3591cef --destination-cidr-block 0.0.0.0/0 --gateway-id igw-0222afa6c521d6443`

      - Validate your route table has correct config, 1 local route and 1 Interent gateway route: `aws ec2 describe-route-tables --route-table-id rtb-0c48fa00ce3591cef`

    - Associate route table to subnet to allow Internet traffic into subnet: `aws ec2 associate-route-table --route-table-id rtb-0c48fa00ce3591cef --subnet-id subnet-09dab358e9c6d85b5`

      - AssociationId: `rtbassoc-066d507cafbbd9ad6`

- Create a security group in the VPC that will allow you access on ssh port 22 and will allow browser access to your Node application
  - `aws ec2 create-security-group --group-name secgroup-aws-exercises --description AWS --vpc-id vpc-0b40f5d04d3027c89`
    - GroupId: `sg-0e19b87b6580569df`
  - `aws ec2 authorize-security-group-ingress --group-id sg-0e19b87b6580569df --protocol tcp --port 22 --cidr 172.56.70.39/32`
    - `ssh` is not a valid protocol, used `tcp`
  - `aws ec2 authorize-security-group-ingress --group-id sg-0e19b87b6580569df --protocol tcp --port 3000 --cidr 172.56.70.39/32`
    - Passed my IP for --cidr option to enable my device to SSH and access app via browser.

EXERCISE 4: Create an EC2 instance in the VPC with the security group you created and ssh key file

- Create SSH key-pair: `aws ec2 create-key-pair --key-name aws-exercises --query 'KeyMaterial' --output text > aws-exercises.pem`

  - Stores unencrypted private key in .pem file locally

- Create EC2: `aws ec2 run-instances \
--image-id ami-0230bd60aa48260c6 \
--count 1 \
--instance-type t2.micro \
--key-name aws-exercises \
--security-group-ids sg-0e19b87b6580569df \
--subnet-id subnet-09dab358e9c6d85b5 \
--associate-public-ip-address`

  - Option `--associate-public-ip-address` is a boolean (defaults to `true`) and requests a public IP for an instance launched in a nondefault subnet. Refer to AWS CLI [docs, Example 2](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/ec2/run-instances.html).

  - Validate EC2 is running and get Public IPv4: `aws ec2 describe-instances --instance-id i-0893b2bb7d038e3b3 --query "Reservations[*].Instances[*].{State:State.Name,Address:PublicIpAddress}"`

EXERCISE 5: SSH into the server and install Docker

- NOTE distinction: `upnata` is configured default user/ account running CLI cmds. However, we're ssh'ing into EC2 as `ec2-user`.
- Adjust .pem file permissions to read-only for user only: `chmod 400 aws-exercises.pem`
- `ssh -i aws-exercises.pem ec2-user@54.167.249.139`

- `apt` is not found, `yum` is the found package manager in EC2
  - `sudo yum update`
- To install Docker and run Docker cmds as non-sudo user:
  - `sudo yum install docker`
  - Start daemon and confirm it's running: `sudo service docker start`, `ps aux | grep docker`
    - Solution code `sudo systemctl start docker` seemed to work, but there was an error w/ `docker.sock` that prevented running Docker cmds.
  - Add `ec2-user` to `docker` group: `sudo usermod -aG docker $USER`
    - Run `groups` to see full list
  - Log out and log back into EC2.

Set up Continuous Deployment (i.e. automate latest Docker img deploy to EC2)

- Used my Module 8 `jenkins-exercises` repo instead of https://gitlab.com/twn-devops-bootcamp/latest/09-aws/aws-exercises. My repo contained all the same files.

EXERCISE 6: Add docker-compose to your NodeJS application

- [Instructions](https://gist.github.com/npearce/6f3c7826c7499587f00957fee62f8ee9#docker-compose-install) to install docker-compose on EC2

EXERCISE 7: Add "deploy to EC2" stage to Jenkinsfile from previous exercise’s project

- Configure SSH creds for Jenkins to SSH into EC2 on global store for `aws-multibranch-pipeline-module-9-exercises` pipeline.

  - Generate "Pipeline Syntax" for sshagent plugin to use in deploy stage.

- Configure EC2 inbound rule to allow Jenkins IP to ssh into port 22.

  - CLI cmd: `aws ec2 authorize-security-group-ingress --group-id sg-0e19b87b6580569df --protocol tcp --port 22 --cidr 159.65.220.254/32`

- Make sure you're logged into Docker on EC2 server before running deploy stage, or you'll get this error: `Error response from daemon: pull access denied for upnata/module-8-bootcamp-node-project, repository does not exist or may require 'docker login': denied: requested access to the resource is denied`

Misc Notes:

- How you resolved `Host key verification failed` issue:
  - Per ChatGPT, the "Host key verification failed" error typically occurs when the SSH client (in this case, Jenkins) detects a mismatch between the expected host key and the key presented by the remote server. This is a (Jenkins?) security feature designed to protect against man-in-the-middle attacks. When you connect to a server for the first time, the server provides its host key, and the client stores this key locally. On subsequent connections, the client compares the presented key with the stored key to verify the server's identity.
  - When you first ssh'ed into your EC2 (`ssh -i aws-exercises.pem ec2-user@54.167.249.139`), you got this feedback:
    `The authenticity of host '54.167.249.139 (54.167.249.139)' can't be established.
ED25519 key fingerprint is SHA256:yQvQgyaiHBthjmtDIXoau0pY6eJmKVw5SXSB3XJxQxQ.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '54.167.249.139' (ED25519) to the list of known hosts.`
  - When you tried `scp` the script file in deploy stage, although the server host key (`ssh-ed25519 SHA256:yQvQgyaiHBthjmtDIXoau0pY6eJmKVw5SXSB3XJxQxQ`) was found, '54.167.249.139' was not found in `/var/jenkins_home/.ssh/known_hosts`.
  - You commented out the `scp` cmds and just ran `sh "ssh -o StrictHostKeyChecking=no ec2-user@54.167.249.139 ${shellCmds}"`, which returned `Warning: Permanently added '54.167.249.139' (ED25519) to the list of known hosts.`
  - For later builds, got `Host '54.167.249.139' is known and matches the ED25519 host key.` and the build finished successfully.

It looks like the EC2 public IP (host) was not added to `known_hosts` the first time.

- Private SSH key provided to SSH client (your local server, Jenkins local server) in .pem file or sshagent plugin.
  - Public SSH key can be found in EC2 server, ~/.ssh/authorized_keys (reference: https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server). The name of the key-pair can be found at the end of the public key (in this case, `aws-exercises`, so you can confirm the correct public key is present on EC2).

EXERCISE 8: Configure access from browser (EC2 Security Group)

- Configured EC2 security group to access app from a browser in Exercise 3: `aws ec2 authorize-security-group-ingress --group-id sg-0e19b87b6580569df --protocol tcp --port 3000 --cidr 172.56.70.39/32`

EXERCISE 9: Configure automatic triggering of multi-branch pipeline
Your team members are creating branches to add new features to the application or fix any issues, so you don't want to build and deploy these half-done features or bug fixes. You want to build and deploy only to the master/main branch. All other branches should only run tests. Add this logic to the Jenkinsfile:

- Add branch based logic to Jenkinsfile

  - Tested conditional logic by creating a branch off `main` called `bugfix/remove-cat-pic` (only change is commenting out cat pic in index.html)

- Add webhook to trigger pipeline automatically

1. Use Jenkins plugin "Multibranch Scan Webhook Trigger" to automate builds on pushes to GL repo

- In multibranch pipeline > Configure > Build Configuration > check "Scan by webhook"
  - Trigger token = input anything
  - Click ? to get URL where Jenkins will accept GL API calls
- Go to GL repo, Settings > Webhooks
  - Input URL from Jenkins, replace JENKINS_URL w/ Jenkins IP:port address
  - Trigger: Push events on all branches
  - Enable "SSL verification"

2. Use Jenkins plugin "Ignore Committer Strategy" to prevent endless build loop
   - In multibranch pipeline > Configure > Branch Sources > Build Strategies > Ignore Committer
   - Add email of `jenkins` user configured in Jenkinsfile
   - Enable "Allow builds when a changeset contains non-ignored author(s)"

AWS resource cleanup via CLI:

- EC2 (ran cmds as `upnata` user)
  - `aws ec2 delete-key-pair --key-name aws-exercises`
  - `aws ec2 terminate-instances --instance-ids i-0893b2bb7d038e3b3`
  - `aws ec2 delete-security-group --group-id sg-0e19b87b6580569df`
- VPC (ran cmds as `upnata` user; Reference: https://docs.aws.amazon.com/vpc/latest/userguide/delete-vpc.html#delete-vpc-cli)
  - `delete-security-group` cmd cannot be run by a user (even w/ admin privileges). Run rest of the cmds below and the VPC's default security group will be deleted as well.
  - `aws ec2 delete-subnet --subnet-id subnet-09dab358e9c6d85b5`
  - `aws ec2 delete-route-table --route-table-id rtb-0c48fa00ce3591cef`
  - `aws ec2 detach-internet-gateway --internet-gateway-id igw-0222afa6c521d6443 --vpc-id vpc-0b40f5d04d3027c89`
  - `aws ec2 delete-internet-gateway --internet-gateway-id igw-0222afa6c521d6443`
  - `aws ec2 delete-vpc --vpc-id vpc-0b40f5d04d3027c89`
- IAM (ran cmds as `twnadmin` user)
  - `aws iam delete-access-key --access-key-id AKIAUKVPVGSOSIYPYDFA --user-name upnata`
  - `aws iam delete-login-profile --user-name upnata`
  - `aws iam remove-user-from-group --group-name devops --user-name upnata`
  - `aws iam delete-user --user-name upnata`
  - `aws iam detach-group-policy --group-name devops --policy-arn arn:aws:iam::aws:policy/IAMUserChangePassword`
  - `aws iam detach-group-policy --group-name devops --policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess`
  - `aws iam detach-group-policy --group-name devops --policy-arn arn:aws:iam::aws:policy/AmazonVPCFullAccess`
  - `aws iam delete-group --group-name devops`
