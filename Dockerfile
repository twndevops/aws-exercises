FROM node:lts-alpine3.18
# create app dir in container filesys
RUN mkdir -p /app
# copy Jenkins' app dir CONTENTS into container filesystem app dir
COPY app/* /app
# set app to be working dir (= home/root dir when you docker exec into container shell)
WORKDIR /app
RUN npm i
CMD npm start

############################################################################################
# Image built from this Dockerfile: 1.0.11-29. Both have the same number of image layers; however, copying the artifact is less clean / results in a heavier container filesystem. 'npm i' runs in test stage, so node_modules in app dir are packaged into the TAR file. Then you 'npm i' again in package dir below in the Docker image build stage, resulting in 2 node_modules dirs. As well, 'npm run test' runs tests in the prev build's package dir and in the curr build's app dir. The TARs and package dirs must be cleaned between builds. It's a lot more pipeline steps to address all of these issues.

# FROM node:lts-alpine3.18
# # copy Jenkins' local app artifact into container filesystem
# COPY app/bootcamp-node-project-*.tgz .
# # unTAR app contents into package dir
# RUN tar zxvf bootcamp-node-project-*.tgz
# # set package as WORKDIR to run later steps
# # must provide a full path for WORKDIR or build will fail at 'npm i'; for example, if you copied the artifact into jsapp dir, the WORKDIR would be /jsapp/package
# WORKDIR /package
# RUN npm i
# CMD npm start