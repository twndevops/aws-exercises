#!/user/bin/env groovy

pipeline {
  // Terminology: agent and stages are sections, tools is a directive
  agent any

  tools {
    nodejs 'nodejs-21.2.0'
  }

  stages {

    stage("run app tests") {
      steps {
        script {
          // Reference: https://www.jenkins.io/doc/pipeline/steps/workflow-durable-task-step/#dir-change-current-directory
          // sh "cd app" does NOT work! By default, "npm i" executes in pipeline workspace dir: /var/jenkins_home/workspace/jenkins-exercises-pipeline_main/.
          dir("app") {
            sh "npm i"
            echo "Executing tests..."
            sh "npm run test"
            // to fail test, change HTML filename and commit change to remote repo
            // by default pipeline should "abort" = skip remaining stages due to test failure
          }
        }
      }
    }

    // increments version in Jenkins' locally checked out repo, not your local package.json!
    stage("increment app version") {
      when {
        branch 'main'
      }
      steps {
        script {
          dir("app") {
            echo "Bumping version..."
            // References: https://docs.npmjs.com/updating-your-published-package-version-number, https://docs.npmjs.com/cli/v9/commands/npm-version 
            sh "npm version minor"
            // following cmd confirms Jenkins' local package.json version was bumped
            // sh "npm pkg get version"
            // Reference: https://www.jenkins.io/doc/pipeline/steps/workflow-basic-steps/#readfile-read-file-from-workspace
            def matches = readFile('package.json') =~ '"version": (.+),'
            // echo "Here are the matches: ${matches[0]}" // matches[0]: ["version": "1.0.1",,  "1.0.1"]
            // env vars are available to all pipeline stages
            env.IMG_NAME = "${matches[0][1]}-$BUILD_NUMBER"
          }
        }
      }
    }

    // Nana's solution skips this stage
    // stage("build TAR") {
    //   steps {
    //     script {
    //       dir("app") {
    //         echo "Building TAR..."
    //         // if a prev version TAR file exists, remove it so Dockerfile's "docker build" has only 1 arg
    //         // if no prev version exists, rm throws error so rest of stages are skipped
    //         // References: https://sentry.io/answers/determine-whether-a-file-exists-or-not-in-bash/, https://www.howtogeek.com/858815/linux-rm-command/
    //         sh "if test -f bootcamp-node-project-*.tgz; then \n rm bootcamp-node-project-*.tgz \n fi"
    //         // package app into TAR with up-to-date package.json version
    //         sh "npm pack"
    //       }
    //     }
    //   }
    // }

    stage("build image, login to Docker, push to private repo") {
      when {
        branch 'main'
      }
      steps {
        script {
            // NOTE: Jenkins requires dir("app") to run cmds in dirs inside pipeline workspace dir (like app). Docker is able to find the app dir inside pipeline workspace dir without dir block.
            echo "Building and pushing image..."
            sh "docker build -t upnata/module-8-bootcamp-node-project:${IMG_NAME} ."
            // extract DockerHub creds separately to be used as vars in sh cmd
            withCredentials([usernamePassword(credentialsId: 'dockerhub-private-repo', usernameVariable: 'USER', passwordVariable: 'PWD')]) {
              sh "echo $PWD | docker login -u $USER --password-stdin"
            }
            sh "docker push upnata/module-8-bootcamp-node-project:${IMG_NAME}"
        }
      }
    }

    stage("deploy/ compose up app on EC2") {
      when {
        branch 'main'
      }
      steps {
        script {
            echo "Deploying on EC2..."
            // define var storing cmd to run project shell script w/ param passing IMG_NAME env var to script
            // def ec2endpt
            def shellCmds = "bash ./aws-exercises.sh ${IMG_NAME}"
            // use SSH creds configured for this build in Jenkins UI to ssh into EC2
            sshagent(['ec2-key-aws-exercises']) {
              // copy script and Compose files from Jenkins' locally checked out repo to EC2
              // login to EC2 -> /home/ec2-user is working dir (run 'pwd')
              // Can use -v flag (=verbose) to help troubleshoot!
              sh "scp aws-exercises.sh ec2-user@54.167.249.139:/home/ec2-user"
              sh "scp docker-compose.yaml ec2-user@54.167.249.139:/home/ec2-user"
              // Jenkins ssh's into EC2
              // set flag to suppress SSH pop-up
              sh "ssh -o StrictHostKeyChecking=no ec2-user@54.167.249.139 ${shellCmds}"
            }
        }
      }
    }

    stage("commit version bump") {
      when {
        branch 'main'
      }
      steps {
        script {
          dir("app") {
            echo "Committing new version..."
            // configure user name and email for Git commit (or Git will throw error)
            sh "git config user.name 'jenkins'"
            sh "git config user.email 'jenkins@exercises.com'"
            sh "git config --list && git status && git branch"
            // extract Git creds separately to be used as vars in sh cmds
            withCredentials([usernamePassword(credentialsId: 'git-creds', usernameVariable: 'USER', passwordVariable: 'PWD')]) {
              // connect Jenkins' locally checked out repo to remote repo and authenticate Jenkins to GL
              sh "git remote set-url origin https://$USER:$PWD@gitlab.com/twndevops/aws-exercises.git"
            }
            sh "git add package.json"
            sh "git commit -m'ci: app minor version bumped'"
            sh "git push origin HEAD:$BRANCH_NAME"
          }
        }
      }
    }

    // be sure to pull remote package.json version update to your local repo before manually triggering next pipeline build

  }

}