#!/user/bin/env bash

# export env var passed from Jenkinsfile as param in exec script cmd
# no spaces around '=' sign!
export LATEST_VERSION=$1

# run container(s) in background
docker-compose up -d